jQuery(document).ready(function () {
    if(jQuery("#section_counter_user").length) {
        jQuery("#section_counter_user").attr("data-status", 0)
        jQuery(window).scroll(function () {
            if (!jQuery("#section_counter_user").data('status')) {
                if (detectScrolling(".site-footer__reviews")) {
                    jQuery("#section_counter_user").data("status", 1)
                    jQuery.ajax({
                        url: request_counter.ajaxUrl,
                        data: {
                            action: 'counter_user',
                        },
                        success: function (result) {
                            jQuery("#section_counter_user").html(result);
                        },
                        error: function (errorThrown) {
                            console.log('fail counter');
                            console.log(errorThrown);
                        }
                    });
                }
            }
        });
    }
});

function detectScrolling(element) {
    let top_of_element = jQuery(element).offset().top;
    let bottom_of_element = jQuery(element).offset().top + jQuery(element).outerHeight();
    let bottom_of_screen = jQuery(window).scrollTop() + jQuery(window).innerHeight();
    let top_of_screen = jQuery(window).scrollTop();

    return (bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element);
}