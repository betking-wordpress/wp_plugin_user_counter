<?php

add_action('wp_ajax_nopriv_counter_user', 'ajaxCounterUser');
add_action('wp_ajax_counter_user', 'ajaxCounterUser');


function ajaxCounterUser()
{
    if(is_active_sidebar( 'user_count' )){
        echo get_sidebar_counter();
    }
    echo "";
    die();
}

function counter_scripts() {
    wp_register_script('counter-user-js', "/wp-content/plugins/wp_plugin_user_count/counter_user.js",array(), 1.3, true
    );
    wp_localize_script( 'counter-user-js', 'request_counter', array( 'ajaxUrl' => admin_url( 'admin-ajax.php' )));

    wp_enqueue_script('counter-user-js');
}

add_action( 'wp_footer', 'counter_scripts' );

function get_sidebar_counter(){
    ob_start();
    dynamic_sidebar('user_count');
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

//DEFER JS
add_filter( 'script_loader_tag', function ( $tag, $handle ) {
    $deferScripts = array(
        'counter-user-js',
        'google_gtagjs-js',

    );
    if ( in_array( $handle , $deferScripts) ) {
        return str_replace( ' src', ' defer src', $tag ); // defer the script
    }else{
        return $tag;
    }
}, 10, 3 );